FROM node:8.15.0-alpine

RUN mkdir /app
WORKDIR /app

COPY /src /app/src
COPY /public /app/public
COPY package.json /app/package.json

ENV PATH /app/node_modules/.bin:$PATH

RUN npm install --silent

CMD ["npm", "start"]