## 4Mini News (ReactJS, Docker)
_Disclaimer: This is not a tutorial for ReactJS or Semantic UI or Docker._ 

### Background
Simple dockerized ReactJS SPA that utilizes semantic-ui. This version is created using create-react-app and not configured manually for webpack/babel frameworks.
When you visit localhost:3000, you should see four individual cards displaying news from four sources as shown below.

| USA Today | ESPN | Hacker New | BBC News |
| ------ | ------ | ------ | ------ |
| ------ | ------ | ------ | ------ |


### Steps to run locally (without docker)
* Signup at https://newsapi.org
* `git clone git@gitlab.com:nagjv/4mini-cra-reactjs.git`
* Update your newsapi key in /src/local-properties.js
* `npm install`
* `npm start`
* Open [http://localhost:3000](http://localhost:3000) to view app in the browser.
* The page will reload if you make edits. You will also see any lint errors in the console.


### Steps to run on AWS S3
* Make sure you have AWS account & configured on your Mac/Laptop
* Signup at https://newsapi.org
* `git clone git@gitlab.com:nagjv/4mini-cra-reactjs.git`
* Update your newsapi key in /src/local-properties.js
* `npm install`
* `npm run build`
* _Optional step, to verify locally with build artifacts_
    * `serve -s build` 
        * [http://localhost:3000](http://localhost:3000)
    * `npm start`
        * [http://localhost:7007](http://localhost:7007)
* Upload build artifacts to S3
    * `npm run s3-deploy`
        * Make sure to update bucket name in package.json > s3-deploy
* If your S3 bucket is enabled for static website hosting with appropriate bucket policy, URL for index.html would be: `http://<bucket name>.s3-website.<aws region>.amazonaws.com/index.html`
    * Example: `http://myapp-bucket010417.s3-website.us-west-1.amazonaws.com/index.html`
* **Notes:**
    * To use serve --> `npm install -g serve`
    * Bucket policy I used. You only need Allow policy for S3. I didn't want public access to anything in my private folder, so explicitly specified with Deny policy.
```javascript
    {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "Allow4MiniFolderAccess",
                "Effect": "Allow",
                "Principal": "*",
                "Action": "s3:GetObject",
                "Resource": "arn:aws:s3:::myapp-bucket010417/*"
            },
            {
                "Sid": "DenyNonPublicFolderAccess",
                "Effect": "Deny",
                "Principal": "*",
                "Action": "*",
                "Resource": "arn:aws:s3:::myapp-bucket010417/private/*"
            }
        ]
    }
```

### Steps to run in a docker container
 * Signup at https://newsapi.org
 * `git clone git@gitlab.com:nagjv/4mini-cra-reactjs.git`
 * Update your newsapi key in /src/local-properties.js
 * Build image & launch container:  `docker-compose up -d --build`
 * Open [http://localhost:3001](http://localhost:3001) to view app in the browser.
 * **Notes:** 
    * App in container runs on port 3001. This is intentional and not a typo :blush:
    * Unless you already have `node:8.15.0-alpine`, step 4 will result in 2 additional images (4mini-news & node 8.15.0-alpine)
    * Stop the container: `docker-compose stop` 
    * Restart the container: `docker-compose restart`
    * I prefer `docker-compose`, but you could do with just `docker` as well (more things to remember :wink:)
        * Build image: `docker build -t 4mini .`
        * Launch container: `docker run -it -v ${PWD}:/app -v /app/node_modules -p 3001:3000 --rm 4mini`
# jenkins-cicd

