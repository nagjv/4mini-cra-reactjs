import React, {Component} from 'react'
import logo from './logo.png';
import News from './news';

import {
  Container,
  Image,
  List,
  Menu,
  Segment,
  Table
} from 'semantic-ui-react'


class Layout extends Component {

  constructor(...args) {
    super(...args);
    this.state = {
      news_sources: [
        {id:'usa-today', name:'USA Today'},
        {id:'espn-cric-info', name:'Cricket'},
        {id:'espn', name:'ESPN'},
        {id:'buzzfeed', name:'Buzzfeed'}
      ]
    };
  }

  render() {
    console.log('## Layout render ##');

    const news = this.state.news_sources.map((item, key) =>
      <Table.Cell verticalAlign='top' key={item.id+'-cell'}><News name={item.name} id={item.id}/></Table.Cell>
    );

    return(
      <div>
        <Menu borderless fixed='top' inverted size='huge'>
          <Menu.Item header >
            <Image size='mini' src={logo} style={{ marginRight: '1.0em' }} />
            <strong> 4Mini News </strong>
          </Menu.Item>
        </Menu>

        <Container style={{ marginTop: '5em' }} textAlign='left'>
          <Table basic='very'>
              <Table.Body>
                  <Table.Row id='news-row'>
                     {news}
                  </Table.Row>
              </Table.Body>
          </Table>
        </Container>

        <Segment inverted vertical style={{ margin: '1em 0em 0em', padding: '0em 0em' }}>
          <Container textAlign='center'>
            <List horizontal inverted divided link size='small'>
              <List.Item as='a' href='#'>
                About Me
              </List.Item>
              <List.Item as='a' href='#'>
                Git Link
              </List.Item>
            </List>
          </Container>
        </Segment>
      </div>
    );
  }
}

export default Layout