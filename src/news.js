import React, {Component} from 'react'
import { Card, Feed } from 'semantic-ui-react'

import axios from 'axios'
import { properties } from './local-properties.js';

class News extends Component {
  
  state = {
    news_articles: []
  };
  
  constructor(...args) {
    super(...args);
    this.getData();
  }

  async getData() {
    const news_api_url = "https://newsapi.org/v2/top-headlines?sources="+this.props.id+"&apiKey="+properties.NEWS_KEY;
    console.log(news_api_url);
    try {
      const res = await axios.get(news_api_url);
      this.setState({news_articles: res.data.articles});
    } catch(error) {
      console.log(error);
    }
  }

  render() {
    console.log(this.props.name+'::'+this.props.id);
    return(

        <Card>
          <Card.Content>
            <Card.Header size='small' textAlign='center'>{this.props.name}</Card.Header>
          </Card.Content>
          <Card.Content>
          <Feed>
      {
        this.state.news_articles.map((item, i) =>
            <Feed.Event key={i}>
                <Feed.Content as='a' href={item.url} target='_blank'>
                    <Feed.Extra text>{item.title}</Feed.Extra>
                </Feed.Content>
            </Feed.Event>
        )
      }
        </Feed>
        </Card.Content>
     </Card>

    )

  }

}

export default News 